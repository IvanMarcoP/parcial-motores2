using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class Control_Hud : MonoBehaviour
{
    public static Control_Hud instance;

    public TMPro.TMP_Text puntosText;
    public TMPro.TMP_Text notasAcertadasText;
    public TMPro.TMP_Text multiTexto;
    public TMPro.TMP_Text txtGameOver;
    public TMPro.TMP_Text txtWin;

    int notasAcertadas = 0;
    int multiplicador = 1;
    int puntos = 0;
    int highscore = 0;

    private GameManager gm;

    void Awake()
    {
        instance = this;
    }


    // Start is called before the first frame update
    void Start()
    {
        txtGameOver.enabled = false;
        txtWin.enabled = false;
        puntosText.text = "Puntos: " + puntos.ToString();
        notasAcertadasText.text = notasAcertadas.ToString();
        gm = GameObject.Find("GameManager").GetComponent<GameManager>();
    }

    // Update is called once per frame
    void Update()
    {
        if (GameManager.gameOver)
        {
            txtGameOver.enabled = true;
            GestorDeAudio.instancia.PausarSonido("Cancion 1");
            Time.timeScale = 0;
        }
        else if (GameManager.gameWin)
        {
            txtWin.enabled = true;
            GestorDeAudio.instancia.PausarSonido("Cancion 1");
            Time.timeScale = 0;
        }

        if(highscore < puntos)
        {
            highscore = puntos;
        }

    }

    public void aņadirAcertadas()
    {
        notasAcertadas += 1;
        notasAcertadasText.text = notasAcertadas.ToString();
        multiTexto.text = multiplicador.ToString() + "X";

        if (notasAcertadas >= 24)
        {
            multiplicador = 4;
            multiTexto.text = multiplicador.ToString() + "X";
        }
        else if (notasAcertadas >= 16 && notasAcertadas < 24)
        {
            multiplicador = 3;
            multiTexto.text = multiplicador.ToString() + "X";
        }
        else if (notasAcertadas >= 8 && notasAcertadas < 16)
        {
            multiplicador = 2;
            multiTexto.text = multiplicador.ToString() + "X";
        }
        else
        {
            multiplicador = 1;
            multiTexto.text = multiplicador.ToString() + "X";
        }
    }

    public void resetAcertadas()
    {
        notasAcertadas = 0;
        notasAcertadasText.text = notasAcertadas.ToString();
        multiplicador = 1;
        multiTexto.text = multiplicador.ToString() + "X";
    }

    public void aņadirPuntos()
    {
        puntos += 100 * multiplicador;
        puntosText.text = "Puntos: " + puntos.ToString();
    }

    public void aņadirPuntosBarra()
    {
        puntos += 500 * multiplicador;
        puntosText.text = "Puntos: " + puntos.ToString();
    }
}
