using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Presionar_Tecla : MonoBehaviour
{
    public static Presionar_Tecla instance;

    public KeyCode tecla;
    bool activo = false;
    bool activoBarra = false;
    public GameObject generador;
    public bool createMode;
    private int vida = 50;
    public Slider slider;

    private GameObject nota; 
    private GameObject barra;

    private Renderer objeto;
    private Material cambioMaterial;
    private Color colorObj;

    // Start is called before the first frame update
    void Start()
    {
        instance = this;
        objeto = GetComponent<Renderer>();
        cambioMaterial = objeto.material;
        colorObj = cambioMaterial.color;

        slider.value = vida;
    }

    // Update is called once per frame
    void Update()
    {
        //if (Input.GetMouseButtonDown(0))
        //{
        //    if (gameObject.CompareTag("Botiquin"))
        //    {
        //        Destroy(gameObject);
        //    }
        //}

        if (vida <= 0)
        {
            GameManager.gameOver = true;
        }

        slider.value = vida;

        if (createMode)
        {
            if (Input.GetKeyDown(tecla))
            {
                Instantiate(generador, transform.position, Quaternion.identity);
            }
        }
        else
        {
            if (Input.GetKeyDown(tecla) || Input.GetKeyDown(KeyCode.Space))
            {
                StartCoroutine(ColorTemp());
            }

            if (Input.GetKeyDown(tecla) && activo)
            {
                //nota.SetActive(false);
                Destroy(nota);
                Control_Hud.instance.aņadirPuntos();
                Control_Hud.instance.aņadirAcertadas();
                ganarVidas();
                activo = false;
            }
            else if(Input.GetKeyDown(tecla) && !activo)
            {
                Control_Hud.instance.resetAcertadas();
                PerderVidas();
            }

            if(Input.GetKeyDown(KeyCode.Space) && activoBarra)
            {
                Destroy(barra);
                Control_Hud.instance.aņadirPuntosBarra();
                Control_Hud.instance.aņadirAcertadas();
                ganarVidas();
                activoBarra = false;
            }
        }

    }

    void OnTriggerEnter(Collider collision)
    {
        if( collision.gameObject.tag == "Nota")
        {
            activo = true;
            nota = collision.gameObject;
        }
        if (collision.gameObject.tag == "Barra")
        {
            activoBarra = true;
            barra = collision.gameObject;
        }

        if (collision.gameObject.tag == "Victoria")
        {
            GameManager.gameWin = true;
            Debug.Log("Se activo wl win");
        }
    }

    void OnTriggerExit(Collider collision)
    {
        activo = false;
        activoBarra = false;
        Control_Hud.instance.resetAcertadas();
    }

    private void ganarVidas()
    {
        vida = vida + 5;

        if (vida > 100)
        {
            vida = 100;
        }
    }

    public void PerderVidas()
    {
        vida = vida - 10;
        slider.value = vida;
    }

    IEnumerator ColorTemp()
    {
        cambioMaterial.color = new Color(0, 0, 0);
        yield return new WaitForSeconds(0.1f);
        cambioMaterial.color = colorObj;
    }
}
