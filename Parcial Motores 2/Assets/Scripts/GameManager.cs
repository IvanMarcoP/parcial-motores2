using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{

    public static bool gameOver = false;
    public static bool gameWin = false;

    // Start is called before the first frame update
    void Start()
    {
        gameOver = false;
        gameWin = false;
        GestorDeAudio.instancia.ReiniciarSonido("Cancion 1");
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.R))
        {
            ReloadCurrentScene();
            Time.timeScale = 1;
        }

        if(Input.GetKeyDown(KeyCode.Escape))
        {
            Time.timeScale = 1;
            SceneManager.LoadScene("Main Menu");
            GestorDeAudio.instancia.PausarSonido("Cancion 1");
        }
    }

    public void ReloadCurrentScene()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
}
