using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovimientoNota : MonoBehaviour
{
    private Rigidbody rb;
    public float velocidad;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        rb.velocity = new Vector3(0,0, -velocidad);
    }

    private void OnCollisionEnter(Collision col)
    {
        if(col.gameObject.CompareTag("Pared"))
        {
            Destroy(gameObject);
            Control_Hud.instance.resetAcertadas();
            Presionar_Tecla.instance.PerderVidas();
        }
    }
}
