
using UnityEngine;
using UnityEngine.SceneManagement;

public class ControlMenuPrincipal : MonoBehaviour
{
    public void Jugar(string nivel)
    {
        SceneManager.LoadScene(nivel);
    }

    public void Exit()
    {
        Application.Quit();
    }
}
