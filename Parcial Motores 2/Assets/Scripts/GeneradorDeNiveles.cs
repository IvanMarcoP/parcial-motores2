using UnityEngine;

public class GeneradorDeNiveles : MonoBehaviour 
{ 
    public Texture2D mapa;
    public ColorAPrefab[] colorMappings;
    public bool isVertical;

    void Start() 
    { 
        GenerarNivel();
    }
    
    private void GenerarNivel() 
    {
        for (int x = 0; x < mapa.width; x++) 
        { 
            for (int y = 0; y < mapa.height; y++) 
            { 
                GenerarTile(x, y); 
            }
        }
    } 
    
    void GenerarTile(int x, int y) 
    { 
        Color pixelColor = mapa.GetPixel(x, y); 
        
        if (pixelColor.a == 0) 
        { 
            return; 
        }

        foreach (ColorAPrefab colorMapping in colorMappings)
        {
            if (colorMapping.color.Equals(pixelColor))
            {
                Vector3 position;

                if (isVertical)
                {
                    position = new Vector3(x, y, transform.position.z);
                }
                else
                {
                    position = new Vector3(x, transform.position.y, y);
                }

                Instantiate(colorMapping.prefab, position, Quaternion.identity, transform);
            }
        }
        //foreach (ColorAPrefab colorMapping in colorMappings) 
        //{
        //    if (colorMapping.color.Equals(pixelColor)) 
        //    { 
        //        Vector2 position = new Vector2(x, y); 
        //        Instantiate(colorMapping.prefab, position, Quaternion.identity, transform);
        //    }
        //}
    }
}
