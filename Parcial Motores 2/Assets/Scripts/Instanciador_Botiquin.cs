using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Instanciador_Botiquin : MonoBehaviour
{
    public float tiempo;
    public GameObject botiquin;

    // Start is called before the first frame update
    void Start()
    {
        InvokeRepeating("instanciarBotiquin", 5f, tiempo);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void instanciarBotiquin()
    {
        Vector3 randomPosition = new Vector3(UnityEngine.Random.Range(-1f, 16f), UnityEngine.Random.Range(5f, 8f), UnityEngine.Random.Range(14f, 25f));

        Instantiate(botiquin, randomPosition, Quaternion.identity);
    }
}
